# Case
This is a simple REST application that manages products and orders.

## Tech Stack
- Java 17
- Gradle
- Spring Boot
- Caffeine
- H2

## Building & Running

- To run the tests:
```bash
./gradlew tests
```
After execution a [report](target/reports/tests/test/index.html) will be generated

- To build the `jar`
```bash
./gradlew bootJar
```

- To run the application
```bash
java -jar build/libs/assignment-0.0.1-SNAPSHOT.jar
```
Then the application will start at `:8080`


## Swagger UI
After starting the application access http://localhost:8080/swagger-ui/index.html#/, to get
a small documentation of each endpoint
