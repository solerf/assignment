package org.fsoler.assignment.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalListener;
import com.github.benmanes.caffeine.cache.Scheduler;
import org.fsoler.assignment.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.time.Duration;
import java.util.Collection;
import java.util.Optional;

@Configuration
public class CacheConfig {

    @Bean(value = "removalListener")
    RemovalListener<Object, Object> getRemovalListener(@Qualifier("reservedProductsCache") CacheManager reservedProductsCache) {
        return (key, value, cause) -> Optional.ofNullable(value).map(v -> ((Order) v).getProducts())
                .stream()
                .flatMap(Collection::stream)
                .forEach(po -> {
                    Cache reservedProduts = reservedProductsCache.getCache("reservedProducts");
                    // assuming the first get is safe
                    var newQuantity = Optional.ofNullable(reservedProduts.get(po.getProduct().getId(), Integer.class))
                            .map(cacheValue -> cacheValue - po.getQuantity())
                            .orElse(0);
                    reservedProduts.put(po.getProduct().getId(), newQuantity);
                });
    }

    @Primary
    @Bean(value = "reservedOrdersCache")
    CacheManager reservedOrdersCache(@Value("#{T(java.time.Duration).parse('${app.orders.reserve.expire.duration}')}") Duration reserveExpireDuration,
                                     @Autowired RemovalListener<Object, Object> removalListener) {
        CaffeineCacheManager cm = new CaffeineCacheManager("reservedOrders");
        cm.setCaffeine(Caffeine.newBuilder()
                .expireAfterWrite(reserveExpireDuration)
                .scheduler(Scheduler.systemScheduler())
                .evictionListener(removalListener)
        );
        return cm;
    }

    @Bean(value = "reservedProductsCache")
    CacheManager reservedProductsCache() {
        return new ConcurrentMapCacheManager("reservedProducts");
    }


}
