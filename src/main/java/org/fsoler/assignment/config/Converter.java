package org.fsoler.assignment.config;

public interface Converter<OUT> {

    OUT apply();

    default OUT convert() {
        return this.apply();
    }

}
