package org.fsoler.assignment.controller;

import org.fsoler.assignment.service.OrderService;
import org.fsoler.assignment.service.ProductService;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ControllerErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public @ResponseBody
    Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        errors.put("error", "Validation errors");
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public @ResponseBody
    Map<String, String> handleExceptions(Exception ex) {
        return mapError(ex);
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(value = {ProductService.ProductNameAlreadyExists.class, OrderService.OrderAlreadyPayed.class, OrderService.OrderAlreadyCancelled.class})
    public @ResponseBody
    Map<String, String> handleExceptionsNotAcceptable(RuntimeException ex) {
        return mapError(ex);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = {ProductService.ProductNotFound.class, OrderService.OrderNotFound.class})
    public @ResponseBody
    Map<String, String> handleExceptionsNotFound(RuntimeException ex) {
        return mapError(ex);
    }

    private Map<String, String> mapError(Exception ex) {
        return Map.of("error", ex.getClass().getSimpleName(), "message", ex.getMessage());
    }

}
