package org.fsoler.assignment.controller;

import org.fsoler.assignment.model.Order;
import org.fsoler.assignment.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping(value = "order")
public class OrderController {

    private final OrderService service;

    @Autowired
    OrderController(OrderService service) {
        this.service = service;
    }

    @Operation(summary = "Creates a new Order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order created"),
            @ApiResponse(responseCode = "406", description = "Order invalid, missing products in stock", content = @Content)
    })
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> create(@RequestBody
                                  @NonNull
                                  @Valid
                                  @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Product") Payloads.OrderInsert orderInsert) {
        Collection<OrderService.MissingProduct> missingProducts = service.create(orderInsert.convert());
        if (missingProducts.isEmpty()) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(missingProducts);
        }
    }

    @Operation(summary = "Pays an Order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order payed"),
            @ApiResponse(responseCode = "400", description = "Errors processing request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Referenced Order not found", content = @Content),
            @ApiResponse(responseCode = "406", description = "Order in invalid status", content = @Content)
    })
    @GetMapping(path = "/pay/{id}")
    ResponseEntity<Object> pay(@PathVariable @NonNull @Valid @Parameter(description = "Order id") Long id) {
        service.pay(id);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Cancels an Order")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order cancelled"),
            @ApiResponse(responseCode = "400", description = "Errors processing request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Referenced Order not found", content = @Content),
            @ApiResponse(responseCode = "406", description = "Order in invalid status", content = @Content)
    })
    @DeleteMapping("/cancel/{id}")
    ResponseEntity<Object> cancel(@PathVariable @NonNull @Valid @Parameter(description = "Order id") Long id) {
        service.cancel(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/list")
    @ResponseBody
    Collection<Order> list() {
        return service.list();
    }
}
