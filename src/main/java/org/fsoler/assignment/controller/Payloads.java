package org.fsoler.assignment.controller;

import org.fsoler.assignment.config.Converter;
import org.fsoler.assignment.model.Order;
import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.model.ProductOrder;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.Collection;
import java.util.stream.Collectors;

public final class Payloads {

    public record ProductInsert(
            @NotNull(message = "Name is mandatory")
            @NotBlank(message = "Name is mandatory")
            String name,

            @NotNull(message = "Quantity Stock is mandatory")
            @Min(value = 1, message = "Min Quantity in Stock is 1")
            Integer quantityStock,

            @NotNull(message = "Price is mandatory")
            @DecimalMin(value = "0.1", message = "Min Price 0.1")
            Double price) implements Converter<Product> {

        @Override
        public Product apply() {
            return new Product(null, name, quantityStock, price);
        }
    }

    public record ProductUpdate(
            @NotNull(message = "Id is mandatory")
            Long id,

            @NotNull(message = "Name is mandatory")
            @NotBlank(message = "Name is mandatory")
            String name,

            @NotNull(message = "Quantity Stock is mandatory")
            @Min(value = 1, message = "Min Quantity in Stock is 1")
            Integer quantityStock,

            @NotNull(message = "Price is mandatory")
            @DecimalMin(value = "0.1", message = "Min Price 0.1")
            Double price) implements Converter<Product> {

        @Override
        public Product apply() {
            return new Product(null, name, quantityStock, price);
        }
    }

    public record OrderInsert(
            @NotNull(message = "Products are mandatory") @NotEmpty(message = "Products are mandatory") Collection<@Valid ProductOrderInsert> products)
            implements Converter<Order> {

        @Override
        public Order apply() {
            var p = products.stream().map(ProductOrderInsert::convert).collect(Collectors.toSet());
            return new Order(p);
        }
    }

    public record ProductOrderInsert(
            @NotNull(message = "Product Id is mandatory")
            @Min(value = 1, message = "Min Quantity in Stock is 1")
            Long productId,

            @NotNull(message = "Quantity is mandatory")
            @Min(value = 1, message = "Min Quantity is 1")
            Integer quantity) implements Converter<ProductOrder> {

        @Override
        public ProductOrder apply() {
            return new ProductOrder(null, new Product(productId, null, null, null), quantity);
        }
    }
}
