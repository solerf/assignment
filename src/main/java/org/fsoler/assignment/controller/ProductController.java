package org.fsoler.assignment.controller;

import org.fsoler.assignment.controller.Payloads.ProductInsert;
import org.fsoler.assignment.controller.Payloads.ProductUpdate;
import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@RestController
@RequestMapping(value = "product")
public class ProductController {

    private final ProductService service;

    @Autowired
    ProductController(ProductService service) {
        this.service = service;
    }

    @Operation(summary = "Adds a new Product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product added"),
            @ApiResponse(responseCode = "400", description = "Errors processing request", content = @Content)
    })
    @PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> add(@Valid
                               @NonNull
                               @RequestBody
                               @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Product") ProductInsert product) {
        service.saveOrUpdate(product.convert());
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Updates an existing Product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product updated"),
            @ApiResponse(responseCode = "400", description = "Errors processing request", content = @Content)
    })
    @PutMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> update(@Valid
                                  @NonNull
                                  @RequestBody
                                  @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Product") ProductUpdate product) {
        service.saveOrUpdate(product.convert());
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Deletes an existing Product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product deleted"),
            @ApiResponse(responseCode = "400", description = "Errors processing request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Product not found", content = @Content)
    })
    @DeleteMapping(path = "/{id}")
    ResponseEntity<Object> delete(@Parameter(description = "Product id")
                                  @Valid
                                  @NotNull
                                  @NotBlank
                                  @PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

    @Operation(summary = "Lists all existing Product")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Product deleted",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Product.class))}
            ),
            @ApiResponse(responseCode = "400", description = "Errors processing request", content = @Content)
    })
    @GetMapping(path = "/list")
    @ResponseBody
    Collection<Product> list() {
        return service.list();
    }

}
