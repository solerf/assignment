package org.fsoler.assignment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@javax.persistence.Entity(name = "orders")
public final class Order {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(mappedBy = "product")
    private Collection<ProductOrder> products;

    private Double amount;
    private LocalDateTime createAt = LocalDateTime.now(); //this must go to a pre persist
    private LocalDateTime payedAt;
    private LocalDateTime cancelledAt;

    public Order(Collection<ProductOrder> products) {
        this.products = products;
        this.createAt = LocalDateTime.now();
        this.amount = calculateAmount();
    }

    public boolean isPayed() {
        return Objects.nonNull(payedAt);
    }

    public boolean isCancelled() {
        return Objects.nonNull(cancelledAt);
    }

    public Double calculateAmount() {
        return products.stream()
                .map(po -> Optional.ofNullable(po.getProduct().getPrice()).orElse(0D) * po.getQuantity())
                .mapToDouble(a -> a)
                .sum();
    }

    public Collection<Long> getProductIds() {
        return this.products.stream()
                .map(po -> po.getProduct().getId())
                .collect(Collectors.toSet());
    }
}
