package org.fsoler.assignment.service;

import org.fsoler.assignment.model.Order;
import org.fsoler.assignment.model.ProductOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderReservationService {

    private final Cache reservedOrdersCache;
    private final Cache reservedProductsCache;

    @Autowired
    public OrderReservationService(@Qualifier("reservedOrdersCache") CacheManager reservedOrdersCacheManager,
                                   @Qualifier("reservedProductsCache") CacheManager reservedProductsCacheManager) {
        this.reservedOrdersCache = reservedOrdersCacheManager.getCache("reservedOrders");
        this.reservedProductsCache = reservedProductsCacheManager.getCache("reservedProducts");
    }

    @CachePut(cacheManager = "reservedOrdersCache", value = "reservedOrders", key = "#result.getId()")
    public Order reserve(Order order) {
        reserveProducts(order.getProducts());
        return order;
    }

    private void reserveProducts(Collection<ProductOrder> productsOrder) {
        productsOrder.forEach(po -> {
            var pId = po.getProduct().getId();
            var currentQuantity = Optional.ofNullable(reservedProductsCache.get(pId, Integer.class))
                    .orElse(0);
            reservedProductsCache.put(pId, po.getQuantity() + currentQuantity);
        });
    }

    public Map<Long, Integer> getReservedQuantity(Set<Long> productIds) {
        return productIds.stream()
                .map(pId -> Map.entry(pId, Optional.ofNullable(reservedProductsCache.get(pId, Integer.class)).orElse(0)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    @CacheEvict(cacheManager = "reservedOrdersCache", value = "reservedOrders")
    public void cancel(Long orderId) {
        Optional.ofNullable(reservedOrdersCache.get(orderId, Order.class))
                .ifPresent(o -> cancelProducts(o.getProducts()));
    }

    private void cancelProducts(Collection<ProductOrder> productsOrder) {
        productsOrder.forEach(po -> {
            var pId = po.getProduct().getId();
            var currentQuantity = Optional.ofNullable(reservedProductsCache.get(pId, Integer.class))
                    .orElse(0);
            reservedProductsCache.put(pId, currentQuantity - po.getQuantity());
        });
    }
}
