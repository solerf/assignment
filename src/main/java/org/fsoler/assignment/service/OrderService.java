package org.fsoler.assignment.service;

import org.fsoler.assignment.model.Order;
import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.model.ProductOrder;
import org.fsoler.assignment.repository.OrderRepository;
import org.fsoler.assignment.repository.ProductOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

@Service
public class OrderService {

    public record MissingProduct(Long productId, String productName, Integer missingQuantity) {
    }

    public static final class OrderNotFound extends RuntimeException {
        public OrderNotFound(String message) {
            super(message);
        }
    }

    public static final class OrderAlreadyPayed extends RuntimeException {
        public OrderAlreadyPayed(String message) {
            super(message);
        }
    }

    public static final class OrderAlreadyCancelled extends RuntimeException {
        public OrderAlreadyCancelled(String message) {
            super(message);
        }
    }

    private final OrderRepository orderRepository;
    private final ProductOrderRepository productOrderRepository;
    private final ProductService productService;
    private final OrderReservationService orderReservationService;

    @Autowired
    public OrderService(OrderRepository orderRepository,
                        ProductOrderRepository productOrderRepository,
                        ProductService productService,
                        OrderReservationService orderReservationService) {
        this.orderRepository = orderRepository;
        this.productOrderRepository = productOrderRepository;
        this.productService = productService;
        this.orderReservationService = orderReservationService;
    }

    @Transactional(rollbackOn = Exception.class)
    public Collection<MissingProduct> create(Order order) {
        var productsOrder = findProducts(order);
        var productsMissingStock = validateProductsQuantities(productsOrder);
        if (productsMissingStock.isEmpty()) {
            placeOrder(order, productsOrder);
            return Collections.emptySet();
        } else {
            return productsMissingStock;
        }
    }

    private Collection<ProductOrder> findProducts(Order order) {
        var products = productService
                .findAll(order.getProducts().stream()
                        .map(ProductOrder::getId)
                        .collect(Collectors.toSet())
                )
                .stream()
                .collect(Collectors.toMap(Product::getId, Function.identity()));

        var productsOrder = order.getProducts().stream().toList();
        return IntStream.range(0, order.getProducts().size())
                .mapToObj(idx -> {
                    var po = productsOrder.get(idx);
                    return new ProductOrder(null, po.getProduct(), po.getQuantity());
                })
                .collect(Collectors.toSet());
    }

    private Collection<MissingProduct> validateProductsQuantities(Collection<ProductOrder> products) {
        var ids = products.stream().map(ProductOrder::getProduct).map(Product::getId).collect(Collectors.toSet());
        var fromStock = productService.getProductsInStock(ids);
        var fromReserves = orderReservationService.getReservedQuantity(ids);

        Function<ProductOrder, MissingProduct> calculateMissingQuantities = currentProduct -> {
            var productId = currentProduct.getProduct().getId();
            var inStock = Optional.ofNullable(fromStock.get(productId))
                    .map(Product::getQuantityStock)
                    .orElse(0);
            var reserved = fromReserves.getOrDefault(productId, 0);
            var left = (currentProduct.getQuantity() + reserved) - inStock;
            return new MissingProduct(productId, currentProduct.getProduct().getName(), left);
        };
        return products.stream()
                .map(calculateMissingQuantities)
                .filter(mp -> mp.missingQuantity() > 0)
                .collect(Collectors.toSet());
    }

    private void placeOrder(Order order, Collection<ProductOrder> productsOrder) {
        productOrderRepository.saveAll(productsOrder);
        order.setProducts(productsOrder);
        Order saved = orderRepository.save(order);
        orderReservationService.reserve(saved);
    }

    @Transactional(rollbackOn = Exception.class)
    public void cancel(Long id) throws OrderNotFound {
        var orderToCancel = findOrder(id);
        orderToCancel.setCancelledAt(LocalDateTime.now());
        orderRepository.save(orderToCancel);
        orderReservationService.cancel(id);
    }

    @Transactional(rollbackOn = Exception.class)
    public void pay(Long id) throws OrderNotFound {
        var orderToPay = findOrder(id);
        orderToPay.setPayedAt(LocalDateTime.now());
        orderRepository.save(orderToPay);
        orderReservationService.cancel(id);
    }

    public Order findOrder(Long id) {
        var order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFound("Order [" + id + "] not found"));
        if (order.isPayed()) {
            throw new OrderAlreadyPayed("Order [" + id + "] already payed");
        }
        if (order.isCancelled()) {
            throw new OrderAlreadyCancelled("Order [" + id + "] already cancelled");
        }
        return order;
    }

    public Collection<Order> list() {
        return StreamSupport.stream(orderRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }
}
