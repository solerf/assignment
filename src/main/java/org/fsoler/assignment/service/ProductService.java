package org.fsoler.assignment.service;

import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProductService {

    public static final class ProductNotFound extends RuntimeException {
        public ProductNotFound(String message) {
            super(message);
        }
    }

    public static final class ProductNameAlreadyExists extends RuntimeException {
        public ProductNameAlreadyExists(String message) {
            super(message);
        }
    }

    private final ProductRepository repository;
    private final BiPredicate<Long, Long> notSameId = (idNew, idDb) -> Objects.isNull(idNew) || !idDb.equals(idNew);

    @Autowired
    ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    @Transactional(rollbackOn = Exception.class)
    public Product saveOrUpdate(Product product) {
        var nameExists = repository.findByName(product.getName())
                .stream()
                .anyMatch(p -> notSameId.test(product.getId(), p.getId())
                        && product.getName().equalsIgnoreCase(p.getName()));

        if (nameExists) {
            throw new ProductNameAlreadyExists("Product name [" + product.getName() + "] already exists");
        }
        return repository.save(product);
    }

    @Transactional(rollbackOn = Exception.class)
    public void delete(Long id) {
        repository.findById(id)
                .ifPresentOrElse(p -> repository.deleteById(id), () -> {
                    throw new ProductNotFound("Product [" + id + "] not found");
                });
    }

    public Map<Long, Product> getProductsInStock(Collection<Long> productIds) {
        return StreamSupport.stream(repository.findAllById(productIds).spliterator(), false).collect(Collectors.toMap(Product::getId, Function.identity()));
    }

    public Collection<Product> list() {
        return StreamSupport.stream(repository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    public Collection<Product> findAll(Collection<Long> ids) {
        return StreamSupport.stream(repository.findAllById(ids).spliterator(), false).collect(Collectors.toList());
    }
}
