package org.fsoler.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.fsoler.assignment.model.Order;
import org.fsoler.assignment.service.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = {OrderController.class, ControllerErrorHandler.class})
class OrderControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderService;

    @Autowired
    private ObjectMapper objMapper;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void should_create_an_order() throws Exception {
        var o = new Payloads.OrderInsert(Set.of(
                new Payloads.ProductOrderInsert(1L, 1),
                new Payloads.ProductOrderInsert(2L, 1)
        ));

        var payload = objMapper.writeValueAsString(o);
        mockMvc.perform(post("/order/").contentType(MediaType.APPLICATION_JSON).content(payload))
                .andExpect(status().isOk());

        verify(orderService, times(1)).create(any(Order.class));
    }

    @Test
    void should_fail_invalid_when_creating_an_order() throws Exception {
        mockMvc.perform(post("/order/").contentType(MediaType.APPLICATION_JSON).content("{\"abc\": 123}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_pay_an_order() throws Exception {
        mockMvc.perform(get("/order/pay/1"))
                .andExpect(status().isOk());

        verify(orderService, times(1)).pay(1L);
    }

    @Test
    void should_fail_pay_when_order_not_found() throws Exception {
        doThrow(new OrderService.OrderNotFound(""))
                .when(orderService).pay(anyLong());

        mockMvc.perform(get("/order/pay/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_fail_if_invalid_payload_when_paying_an_order() throws Exception {
        mockMvc.perform(get("/order/pay/X"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_cancel_an_order() throws Exception {
        mockMvc.perform(delete("/order/cancel/1"))
                .andExpect(status().isOk());

        verify(orderService, times(1)).cancel(1L);
    }

    @Test
    void should_fail_if_invalid_payload_canceling_an_order() throws Exception {
        mockMvc.perform(delete("/order/cancel/X"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_fail_cancel_when_order_not_found() throws Exception {
        doThrow(new OrderService.OrderNotFound(""))
                .when(orderService).cancel(anyLong());

        mockMvc.perform(delete("/order/cancel/1"))
                .andExpect(status().isNotFound());
    }

}
