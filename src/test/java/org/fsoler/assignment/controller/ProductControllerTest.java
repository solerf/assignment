package org.fsoler.assignment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@ContextConfiguration(classes = {ProductController.class, ControllerErrorHandler.class})
class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @Autowired
    private ObjectMapper objMapper;

    @BeforeEach
    public void setUp(WebApplicationContext webApplicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void should_delete_product_by_id() throws Exception {
        mockMvc.perform(delete("/product/1"))
                .andExpect(status().isOk());

        verify(productService, times(1)).delete(1L);
    }

    @Test
    void should_fail_when_missing_delete_product_id() throws Exception {
        mockMvc.perform(delete("/product/"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_fail_when_delete_product_id_invalid() throws Exception {
        mockMvc.perform(delete("/product/A"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("MethodArgumentTypeMismatchException")));

        verify(productService, never()).delete(1L);
    }

    @Test
    void should_add_product() throws Exception {
        Product p = new Product(null, "p1", 5, 1d);
        String param = objMapper.writer().writeValueAsString(p);
        mockMvc.perform(post("/product/").contentType(MediaType.APPLICATION_JSON).content(param))
                .andExpect(status().isOk());

        verify(productService, times(1)).saveOrUpdate(any(Product.class));
    }

    @Test
    void should_fail_save_when_product_name_exists() throws Exception {
        when(productService.saveOrUpdate(any(Product.class)))
                .thenThrow(new ProductService.ProductNameAlreadyExists(""));

        Product p = new Product(null, "p1", 5, 1d);
        String param = objMapper.writer().writeValueAsString(p);
        mockMvc.perform(post("/product/").contentType(MediaType.APPLICATION_JSON).content(param))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void should_fail_add_missing_product() throws Exception {
        mockMvc.perform(post("/product/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(productService, never()).saveOrUpdate(any(Product.class));
    }

    @Test
    void should_fail_delete_when_product_not_found() throws Exception {
        doThrow(new ProductService.ProductNotFound(""))
                .when(productService).delete(anyLong());

        mockMvc.perform(post("/product/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_fail_add_invalid_product() throws Exception {
        mockMvc.perform(post("/product/").contentType(MediaType.APPLICATION_JSON).content("{\"abc\": 123}"))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(content().string(allOf(
                        containsString("Quantity Stock is mandatory"),
                        containsString("Price is mandatory"),
                        containsString("Name is mandatory")
                )));

        verify(productService, never()).saveOrUpdate(any(Product.class));
    }

    @Test
    void should_update_product() throws Exception {
        Product p = new Product(1L, "p1", 5, 1d);
        String param = objMapper.writer().writeValueAsString(p);
        mockMvc.perform(put("/product/").contentType(MediaType.APPLICATION_JSON).content(param))
                .andExpect(status().isOk());

        verify(productService, times(1)).saveOrUpdate(any(Product.class));
    }

    @Test
    void should_fail_update_when_product_name_exists() throws Exception {
        when(productService.saveOrUpdate(any(Product.class)))
                .thenThrow(new ProductService.ProductNameAlreadyExists(""));

        Product p = new Product(1L, "p1", 5, 1d);
        String param = objMapper.writer().writeValueAsString(p);
        mockMvc.perform(put("/product/").contentType(MediaType.APPLICATION_JSON).content(param))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void should_fail_update_missing_product() throws Exception {
        mockMvc.perform(put("/product/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        verify(productService, never()).saveOrUpdate(any(Product.class));
    }

    @Test
    void should_fail_update_invalid_product() throws Exception {
        mockMvc.perform(put("/product/").contentType(MediaType.APPLICATION_JSON).content("{\"abc\": 123}"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(allOf(
                        containsString("Quantity Stock is mandatory"),
                        containsString("Price is mandatory"),
                        containsString("Name is mandatory")
                )));

        verify(productService, never()).saveOrUpdate(any(Product.class));
    }

}
