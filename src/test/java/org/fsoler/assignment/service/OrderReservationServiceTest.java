package org.fsoler.assignment.service;

import org.fsoler.assignment.model.Order;
import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.model.ProductOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(properties = {"app.orders.reserve.expire.duration=PT2S"})
class OrderReservationServiceTest {

    @Autowired
    private OrderReservationService orderReservationService;

    @Test
    void should_update_products_quantity() {
        var product = new Product(1L, "A", 5, 5d);
        var po_1 = new ProductOrder(1L, product, 2);
        var o_1 = new Order(1L, List.of(po_1), 15d, null, null, null);
        var o_2 = new Order(2L, List.of(po_1), 15d, null, null, null);

        orderReservationService.reserve(o_1);
        orderReservationService.reserve(o_2);

        Map<Long, Integer> actual = orderReservationService.getReservedQuantity(Set.of(product.getId()));
        assertThat(actual.get(product.getId()), is(4));

        orderReservationService.cancel(o_2.getId());

        Map<Long, Integer> updated = orderReservationService.getReservedQuantity(Set.of(product.getId()));
        assertThat(updated.get(product.getId()), is(2));
    }

    @Test
    void should_cancel_order_and_products() throws InterruptedException {
        var product_1 = new Product(1L, "A", 5, 5d);
        var po_1 = new ProductOrder(1L, product_1, 2);
        var o = new Order(1L, List.of(po_1), 15d, null, null, null);

        orderReservationService.reserve(o);
        orderReservationService.cancel(o.getId());
        Map<Long, Integer> actual = orderReservationService.getReservedQuantity(Set.of(product_1.getId()));
        assertThat(actual.get(product_1.getId()), is(0));
    }

    @Test
    void should_timeout_reserve_order_and_products() throws InterruptedException {
        var product_1 = new Product(1L, "A", 5, 5d);
        var po_1 = new ProductOrder(1L, product_1, 2);
        var o = new Order(1L, List.of(po_1), 15d, null, null, null);

        orderReservationService.reserve(o);
        Thread.sleep(4000); // expiration is set to 2s

        Map<Long, Integer> actual = orderReservationService.getReservedQuantity(Set.of(product_1.getId()));
        assertThat(actual.get(product_1.getId()), is(0));
    }

    @Test
    void should_return_reserved_products_quantities() {
        var product_1 = new Product(1L, "A", 5, 5d);
        var po_1 = new ProductOrder(1L, product_1, 2);
        var o = new Order(1L, List.of(po_1), 15d, null, null, null);

        orderReservationService.reserve(o);
        Map<Long, Integer> actual = orderReservationService.getReservedQuantity(Set.of(product_1.getId()));
        assertThat(actual.get(product_1.getId()), is(2));
    }

}
