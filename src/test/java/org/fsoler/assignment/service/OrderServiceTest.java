package org.fsoler.assignment.service;

import org.fsoler.assignment.model.Order;
import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.model.ProductOrder;
import org.fsoler.assignment.repository.OrderRepository;
import org.fsoler.assignment.repository.ProductOrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestPropertySource(properties = {"app.orders.cache.timeout=2 SECONDS"})
class OrderServiceTest {

    @MockBean
    private OrderRepository orderRepository;
    @MockBean
    private ProductOrderRepository productOrderRepository;
    @MockBean
    private OrderReservationService orderReservationService;
    @MockBean
    private ProductService productService;

    @Autowired
    private OrderService service;

    private final Product product_1 = new Product(1L, "A", 5, 5d);
    private final Product product_2 = new Product(2L, "B", 2, 5d);
    private final Set<Product> inStock = Set.of(product_1, product_2);
    private final ProductOrder reserved = new ProductOrder(1L, product_1, 1);

    @BeforeEach
    void beforeEach() {
        Set<Long> productIds = inStock.stream().map(Product::getId).collect(Collectors.toSet());

        when(productService.getProductsInStock(productIds))
                .thenReturn(inStock.stream().collect(Collectors.toMap(Product::getId, Function.identity())));

        when(orderReservationService.getReservedQuantity(productIds))
                .thenReturn(Map.of(reserved.getProduct().getId(), reserved.getQuantity()));

        when(productService.findAll(any(Collection.class))).thenReturn(Set.of(product_1, product_2));
    }

    @Test
    void should_not_create_order_and_return_products_if_missing_in_stock() {
        var po_1 = new ProductOrder(null, product_1, 5);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, null, null, null);
        Collection<OrderService.MissingProduct> products = service.create(o);

        assertThat(products, hasSize(1));
        assertThat(products, hasItems(new OrderService.MissingProduct(1L, "A", 1)));

        verify(productOrderRepository, never()).saveAll(o.getProducts());
        verify(orderRepository, never()).save(o);
        verify(orderReservationService, never()).reserve(o);
    }

    @Test
    void should_not_create_order_and_return_products_if_below_stock_plus_reserved() {
        var po_1 = new ProductOrder(null, product_1, 5);
        var po_2 = new ProductOrder(null, product_2, 4);
        var o = new Order(1L, List.of(po_1, po_2), 15d, null, null, null);
        Collection<OrderService.MissingProduct> products = service.create(o);

        assertThat(products, hasSize(2));
        assertThat(products, hasItems(
                new OrderService.MissingProduct(1L, "A", 1),
                new OrderService.MissingProduct(2L, "B", 2)
        ));

        verify(productOrderRepository, never()).saveAll(o.getProducts());
        verify(orderRepository, never()).save(o);
        verify(orderReservationService, never()).reserve(o);
    }

    @Test
    void should_create_order_if_products_in_stock() {
        var po_1 = new ProductOrder(null, product_1, 2);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, null, null, null);
        var expected = new Order(1L, List.of(po_1, po_2), o.calculateAmount(), null, null, null);

        when(orderRepository.save(any(Order.class))).thenReturn(expected);
        Collection<OrderService.MissingProduct> products = service.create(o);

        assertThat(products, empty());
        verify(orderReservationService, times(1)).getReservedQuantity(Set.of(product_1.getId(), product_2.getId()));
        verify(productOrderRepository, times(1)).saveAll(any(Collection.class));
        verify(orderRepository, times(1)).save(any(Order.class));
        verify(orderReservationService, times(1)).reserve(expected);
    }

    @Test
    void should_pay_order() {
        var createdAt = LocalDateTime.now();
        var po_1 = new ProductOrder(null, product_1, 2);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, createdAt, null, null);

        when(orderRepository.findById(1L)).thenReturn(Optional.of(o));
        service.pay(o.getId());

        var argCapture = ArgumentCaptor.forClass(Order.class);
        verify(orderRepository, times(1)).save(argCapture.capture());
        verify(orderReservationService, times(1)).cancel(o.getId());

        var actual = argCapture.getValue();
        assertTrue(actual.isPayed());
    }

    @Test
    void should_fail_pay_order_if_already_payed() {
        var createdAt = LocalDateTime.now();
        var po_1 = new ProductOrder(null, product_1, 2);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, createdAt, createdAt.plusMinutes(10), null);

        when(orderRepository.findById(1L)).thenReturn(Optional.of(o));
        assertThrowsExactly(OrderService.OrderAlreadyPayed.class, () -> service.pay(o.getId()));
    }

    @Test
    void should_fail_pay_order_if_missing() {
        when(orderRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrowsExactly(OrderService.OrderNotFound.class, () -> service.pay(1L));
    }

    @Test
    void should_fail_pay_order_if_already_cancelled() {
        var createdAt = LocalDateTime.now();
        var po_1 = new ProductOrder(null, product_1, 2);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, createdAt, null, createdAt.plusMinutes(10));

        when(orderRepository.findById(1L)).thenReturn(Optional.of(o));
        assertThrowsExactly(OrderService.OrderAlreadyCancelled.class, () -> service.pay(o.getId()));
    }

    @Test
    void should_cancel_order() {
        var createdAt = LocalDateTime.now();
        var po_1 = new ProductOrder(null, product_1, 2);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, createdAt, null, null);

        when(orderRepository.findById(1L)).thenReturn(Optional.of(o));
        service.cancel(o.getId());

        var argCapture = ArgumentCaptor.forClass(Order.class);
        verify(orderRepository, times(1)).save(argCapture.capture());
        verify(orderReservationService, times(1)).cancel(o.getId());

        var actual = argCapture.getValue();
        assertTrue(actual.isCancelled());
    }

    @Test
    void should_fail_cancel_order_if_already_payed() {
        var createdAt = LocalDateTime.now();
        var po_1 = new ProductOrder(null, product_1, 2);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, createdAt, createdAt.plusMinutes(10), null);

        when(orderRepository.findById(1L)).thenReturn(Optional.of(o));
        assertThrowsExactly(OrderService.OrderAlreadyPayed.class, () -> service.cancel(o.getId()));
    }

    @Test
    void should_fail_cancel_order_if_missing() {
        when(orderRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrowsExactly(OrderService.OrderNotFound.class, () -> service.cancel(1L));
    }

    @Test
    void should_fail_cancel_order_if_already_cancelled() {
        var createdAt = LocalDateTime.now();
        var po_1 = new ProductOrder(null, product_1, 2);
        var po_2 = new ProductOrder(null, product_2, 1);
        var o = new Order(1L, List.of(po_1, po_2), 15d, createdAt, null, createdAt.plusMinutes(10));

        when(orderRepository.findById(1L)).thenReturn(Optional.of(o));
        assertThrowsExactly(OrderService.OrderAlreadyCancelled.class, () -> service.cancel(o.getId()));
    }

}
