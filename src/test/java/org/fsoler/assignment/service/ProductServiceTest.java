package org.fsoler.assignment.service;

import org.fsoler.assignment.model.Product;
import org.fsoler.assignment.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProductServiceTest {

    @MockBean
    private ProductRepository repository;

    @Autowired
    private ProductService service;

    @Test
    void should_save_a_product() {
        Product p = new Product(null, "A", 1, 1d);
        Product persisted = new Product(1L, "", 1, 1d);

        when(repository.findByName("A")).thenReturn(Optional.empty());
        when(repository.save(any(Product.class))).thenReturn(persisted);

        Product product = service.saveOrUpdate(p);
        assertThat(product, equalTo(persisted));
        verify(repository, times(1)).save(p);
    }

    @Test
    void should_fail_save_when_product_name_exists() {
        Product p = new Product(null, "A", 1, 1d);
        Product persisted = new Product(1L, "A", 1, 1d);

        when(repository.findByName("A")).thenReturn(Optional.of(persisted));
        assertThrowsExactly(ProductService.ProductNameAlreadyExists.class, () -> service.saveOrUpdate(p));
    }

    @Test
    void should_update_a_product() {
        Product p = new Product(1L, "B", 1, 1d);
        Product persisted = new Product(1L, "C", 1, 1d);

        when(repository.findByName("B")).thenReturn(Optional.empty());
        when(repository.save(any(Product.class))).thenReturn(persisted);

        Product product = service.saveOrUpdate(p);
        assertThat(product, equalTo(persisted));
        verify(repository, times(1)).save(p);
    }

    @Test
    void should_fail_update_when_product_name_exists() {
        Product p = new Product(1L, "A", 1, 1d);
        Product persisted = new Product(2L, "A", 1, 1d);

        when(repository.findByName("A")).thenReturn(Optional.of(persisted));
        assertThrowsExactly(ProductService.ProductNameAlreadyExists.class, () -> service.saveOrUpdate(p));
    }

    @Test
    void should_return_products_and_quantities_in_stock() {
        Product p1 = new Product(1L, "A", 1, 1d);
        Product p2 = new Product(2L, "B", 3, 1d);
        Product p3 = new Product(3L, "C", 4, 1d);
        Set<Long> ids = Set.of(1L, 2L, 3L);
        when(repository.findAllById(ids)).thenReturn(Set.of(p1, p2, p3));

        Map<Long, Product> stock = service.getProductsInStock(ids);
        assertThat(stock, equalTo(Map.of(
                1L, p1,
                2L, p2,
                3L, p3
        )));
    }
}
